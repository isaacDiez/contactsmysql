import os
from flask import Flask, render_template, request, redirect, url_for, flash
from flaskext.mysql import MySQL


app = Flask(__name__)
# necesaria para asegurar las sessiones y que es la app la que las crea
app.secret_key = os.urandom(12).hex()

mysql = MySQL()

# Configuracion de la DB
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'guesitos'
app.config['MYSQL_DATABASE_DB'] = 'flaskcontacts'
mysql.init_app(app)


@app.route('/')
def index ():
    conn = mysql.connect()
    cursor =conn.cursor()
    cursor.execute('SELECT * FROM contacts')
    data = cursor.fetchall()
    return render_template('index.html', contacts = data)

@app.route('/add_contact', methods=['POST'])
def add_contact():    
    if request.method == 'POST':
        fullname = request.form ['fullname']
        phone = request.form ['phone']
        email = request.form ['email']
        conn = mysql.connect()
        cursor =conn.cursor()
        cursor.execute('INSERT INTO contacts (fullname, phone, email) VALUES (%s, %s, %s)', (fullname,phone,email))
        conn.commit()
        # insertar mensajes entre vistas
        flash('Contact Add successfully')
        return redirect(url_for('index'))

@app.route('/edit/<id>')
def get_contact(id ):
    conn = mysql.connect()
    cursor =conn.cursor()
    cursor.execute('SELECT * FROM contacts WHERE id = %s',(id))
    data = cursor.fetchall()
    return render_template('edit-contact.html',contact = data[0])
def edit_contact():    
    return 'add_contact'

@app.route('/update/<string:id>', methods = ['POST'])
def update_contact(id): 
    if request.method == 'POST':
        fullname = request.form['fullname']
        phone = request.form['phone']
        email = request.form['email']
        conn = mysql.connect()
        cursor =conn.cursor()
        cursor.execute("""
            UPDATE contacts
            SET fullname = %s,
                email = %s,
                phone = %s
            WHERE 
                id = %s
        
        """, (fullname, email, phone, id))
        conn.commit()
        flash('Contact Update successfully')
        return redirect(url_for('index'))

@app.route('/delete/<string:id>')
def delete_contact(id): 
    conn = mysql.connect()
    cursor =conn.cursor()
    cursor.execute('DELETE FROM contacts WHERE id = {0}'.format(id))
    conn.commit()
    # insertar mensajes entre vistas
    flash('Contact REMOVE successfully')
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(port='5000',host='0.0.0.0', debug=True)